<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crypto extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->columns = array();
        if ($this->session->userdata['logged_in'] == FALSE){
            redirect(base_url().'main'); //if session is not there, redirect to login page
        }
        $this->load->model('Crypto_model');
        $this->load->helper('form');
        $this->load->dbforge();
        $this->initialize();
    }

    private function initialize()
    {
        /*check table*/
        if(!$this->Crypto_model->check_table()){
            redirect('main/logout');
        }

        /*get column from table hitbtc*/
        $this->columns_all = $this->Crypto_model->field_data();

        /*remove id from column*/
        $inc = 0;
        $columns = array();
        foreach($this->columns_all as $each){
            if($each->name != 'id'){
                $columns[$inc] = $each;
                $inc++;
            }
        }

        $this->columns = $columns;
        $this->table_normal = $this->Crypto_model->is_table_normal();
        $this->enable_button_exist = $this->Crypto_model->is_enable_button_exist();
    }

	public function index()
	{
        $data['all_table'] = $this->Crypto_model->get_all_table();
	    $data['crypto_name'] = $this->uri->segment(2);
	    $data['columns'] = $this->columns;
	    $data['table_normal'] = $this->table_normal;
        $data['enable_button_exist'] = $this->enable_button_exist;
        $this->load->view("crypto", $data);
	}

    function is_decimal( $val )
    {
//        if(preg_match('/^[0-9]{1,6}+(\.[0-9]{1,5})?$/', $val)){
        if(preg_match('/^[0-9]{1,6}(\.[0-9]{1,5})?$/', $val)){
            return true;
        }else{
            return false;
        }
    }

    function is_number( $val )
    {
        if(preg_match('/^[0-9]+$/', $val)){
            return true;
        }else{
            return false;
        }
    }


	public function fetch()
    {
        $params['draw'] = $this->input->post('draw');
        $params['columns'] = $this->input->post('columns');
        $params['start'] = $this->input->post('start');
        $params['length'] = $this->input->post('length');
        $params['search'] = $this->input->post('search');
        $params['order'] = $this->input->post('order');

        $query = $this->Crypto_model->get($params);

        $data = array();
        if($this->table_normal) {
            foreach ($query['result']->result() as $each) {
                $checked = '';
                $btn_start_stop = '';
                $sub_array = array();

                foreach ($this->columns as $each_col) {
                    $col = $each_col->name;
                    switch ($col) {
                        case 'enable':
                            $checked = ($each->enable == '1') ? 'checked' : '';
                            $sub_array[] = '<div class="update"><input type="checkbox" data-id="' . $each->id . '" data-column="' . $col . '" ' . $checked . ' disabled /></div>';
                            if ($each->enable == '1') {
                                $btn_start_stop = '<button type="button" name="start_stop" class="btn btn-secondary btn-xs start_stop" id="' . $each->id . '">Stop</button> ';
                            } else {
                                $btn_start_stop = '<button type="button" name="start_stop" class="btn btn-success btn-xs start_stop" id="' . $each->id . '">Start</button> ';
                            }
                            break;
                        default:
                            $sub_array[] = '<div class="update" data-id="' . $each->id . '" data-column="' . $col . '">' . $each->$col . '</div>';
                    }
                }
                $sub_array[] = $btn_start_stop . '<button type="button" name="edit" class="btn btn-info btn-xs edit" id="' . $each->id . '">Edit</button>
                    <button type="button" name="save" class="btn btn-warning btn-xs save" disabled id="' . $each->id . '">Save</button>
                    <button type="button" name="delete" class="btn btn-danger btn-xs delete" id="' . $each->id . '">Delete</button>';
                $data[] = $sub_array;
            }
        }else{
            foreach ($query['result']->result() as $each) {
                $sub_array = array();
                foreach ($this->columns as $each_col) {
                    $col = $each_col->name;
                    $sub_array[] = '<div class="update" data-column="' . $col . '">' . $each->$col . '</div>';
                }
                $data[] = $sub_array;
            }
        }


        $query_all = $this->Crypto_model->get_all();

        $output = array(
            "draw"    => intval($_POST["draw"]),
            "recordsTotal"  =>  $query_all->num_rows(),
            "recordsFiltered" => $query['number_filter_row'],
            "data"    => $data
        );

        echo json_encode($output);

    }

    public function insert()
    {
        $params = array();

        if(!$this->table_normal) {
            header("HTTP/1.0 500 Internal Server Error");
            echo "Data Not Inserted.";
            return;
        }

        foreach($this->columns as $each_col){
			$params[$each_col->name] = NULL;
			if(!($each_col->nullable == 1 && $this->input->post($each_col->name) == '')){
				if($this->input->post($each_col->name) == ''){
					header("HTTP/1.0 500 Internal Server Error");
					echo 'Data Not Inserted';
					return;
				}
				/*boolean check*/
				if($each_col->name == 'enable'){
					if(!in_array($this->input->post($each_col->name),array('0','1'))){
						header("HTTP/1.0 500 Internal Server Error");
						echo "Data Not Inserted. ".$each_col->name." value should 0 or 1";
						return;
					}
				}
				/*var_char or char length checked*/
				elseif($each_col->type == 'varchar' OR $each_col->type == 'char'){
					if(strlen($this->input->post($each_col->name)) > $each_col->max_length){
						header("HTTP/1.0 500 Internal Server Error");
						echo "Data Not Inserted. ".$each_col->name."'s length should less than ".$each_col->max_length;
						return;
					}
				}
				/*decimal check*/
				elseif($each_col->type == 'decimal'){
					if(!$this->is_decimal($this->input->post($each_col->name))){
						header("HTTP/1.0 500 Internal Server Error");
						echo "Data Not Inserted. ".$each_col->name." should decimal with length ".($each_col->max_length - $each_col->numeric_scale)." digit And comma length ". $each_col->numeric_scale." digit";
						return;
					}
				}
				/*integer check*/
				elseif($each_col->type == 'int'){
					if(!$this->is_number($this->input->post($each_col->name))){
						header("HTTP/1.0 500 Internal Server Error");
						echo "Data Not Inserted. ".$each_col->name." invalid";
						return;
					}
				}
				$params[$each_col->name] = $this->input->post($each_col->name);
			}
        };

        $query = $this->Crypto_model->insert($params);
        if($query === TRUE){
            echo 'Data Inserted';
            return;
        }else{
            header("HTTP/1.0 500 Internal Server Error");
            echo $query;
            return;
        }
    }

    public function insert_form()
    {
        $params = array();

        if(!$this->table_normal) {
            header("HTTP/1.0 500 Internal Server Error");
            echo "Data Not Inserted.";
            return;
        }

        foreach($this->columns as $each_col){
            if($this->input->post($each_col->name) == ''){
                header("HTTP/1.0 500 Internal Server Error");
                echo 'Data Not Inserted';
                return;
            }
            /*boolean check*/
            if($each_col->name == 'enable'){
                if(!in_array($this->input->post($each_col->name),array('0','1'))){
                    header("HTTP/1.0 500 Internal Server Error");
                    echo "Data Not Inserted. ".$each_col->name." value should 0 or 1";
                    return;
                }
            }
            /*var_char or char length checked*/
            elseif($each_col->type == 'varchar' OR $each_col->type == 'char'){
                if(strlen($this->input->post($each_col->name)) > $each_col->max_length){
                    header("HTTP/1.0 500 Internal Server Error");
                    echo "Data Not Inserted. ".$each_col->name."'s length should less than ".$each_col->max_length;
                    return;
                }
            }
            /*decimal check*/
            elseif($each_col->type == 'decimal'){
                if(!$this->is_decimal($this->input->post($each_col->name))){
                    header("HTTP/1.0 500 Internal Server Error");
                    echo "Data Not Inserted. ".$each_col->name." should decimal with length ".($each_col->max_length - $each_col->numeric_scale)." digit And comma length ". $each_col->numeric_scale." digit";
                    return;
                }
            }
            /*integer check*/
            elseif($each_col->type == 'int'){
                if(!$this->is_number($this->input->post($each_col->name))){
                    header("HTTP/1.0 500 Internal Server Error");
                    echo "Data Not Inserted. ".$each_col->name." invalid";
                    return;
                }
            }
            $params[$each_col->name] = $this->input->post($each_col->name);
        };

        $query = $this->Crypto_model->insert($params);
        if($query === TRUE){
//            echo 'Data Inserted';
//            return;
        }else{
//            header("HTTP/1.0 500 Internal Server Error");
//            echo $query;
//            return;
        }
    }


    public function update()
    {
        $params = array();

        if(!$this->table_normal) {
            header("HTTP/1.0 500 Internal Server Error");
            echo "Data Not Updated.";
            return;
        }
		
        foreach($this->columns_all as $each_col){
			$params[$each_col->name] = NULL;
			if(!($each_col->nullable == 1 && $this->input->post($each_col->name) == '')){
				if($this->input->post($each_col->name) == ''){
					header("HTTP/1.0 500 Internal Server Error");
					echo 'Data Not Updated';
					return;
				}
				/*id (integer) check*/
				if($each_col->name == 'id'){
					if(!$this->is_number($this->input->post($each_col->name)) OR $this->input->post($each_col->name) < 0){
						header("HTTP/1.0 500 Internal Server Error");
						echo "Data Not Updated. ".$each_col->name." invalid";
						return;
					}
				}
				/*boolean check*/
				if($each_col->name == 'enable'){
					if(!in_array($this->input->post($each_col->name),array('0','1'))){
						header("HTTP/1.0 500 Internal Server Error");
						echo "Data Not Updated. ".$each_col->name." value should 0 or 1";
						return;
					}
				}
				/*var_char or char length checked*/
				elseif($each_col->type == 'varchar' OR $each_col->type == 'char'){
					if(strlen($this->input->post($each_col->name)) > $each_col->max_length){
						header("HTTP/1.0 500 Internal Server Error");
						echo "Data Not Updated. ".$each_col->name."'s length should less than ".$each_col->max_length;
						return;
					}
				}
				/*decimal check*/
				elseif($each_col->type == 'decimal'){
					if(!$this->is_decimal($this->input->post($each_col->name))){
						header("HTTP/1.0 500 Internal Server Error");
						echo "Data Not Updated. ".$each_col->name." should decimal with length ".($each_col->max_length - $each_col->numeric_scale)." digit And comma length ".$each_col->numeric_scale." digit";
						return;
					}
				}
				/*integer check*/
				elseif($each_col->type == 'int'){
					if(!$this->is_number($this->input->post($each_col->name))){
						header("HTTP/1.0 500 Internal Server Error");
						echo "Data Not Updated. ".$each_col->name." invalid";
						return;
					}
				}
				$params[$each_col->name] = $this->input->post($each_col->name);
			}
        }

        $query = $this->Crypto_model->update($params);
        if($query === TRUE){
            echo 'Data Updated';
            return;
        }else{
            header("HTTP/1.0 500 Internal Server Error");
            echo $query;
            return;
        }
    }

    public function delete()
    {
        if(!$this->table_normal) {
            header("HTTP/1.0 500 Internal Server Error");
            echo "Data Not Deleted.";
            return;
        }

        $params['id'] = $this->input->post('id');

        /*id (integer) check*/
        if($params['id'] == 'id'){
            if(!$this->is_number($this->input->post($params['id'])) OR $this->input->post($params['id']) < 0){
                header("HTTP/1.0 500 Internal Server Error");
                echo "Data Not Deleted. ".$params['id']->name." invalid";
                return;
            }
        }

        if($this->input->post('id') !== ''){
            $query = $this->Crypto_model->delete($params);
            if($query === TRUE){
                echo 'Data Deleted';
                return;
            }else{
                header("HTTP/1.0 500 Internal Server Error");
                echo $query;
                return;
            }
        }
        header("HTTP/1.0 500 Internal Server Error");
        echo 'Data Not Deleted';
        return;
    }

    public function enable_all()
    {
        if(!$this->table_normal OR !$this->enable_button_exist) {
            header("HTTP/1.0 500 Internal Server Error");
            echo "Data Not Updated.";
            return;
        }

        $query = $this->Crypto_model->enable_all();
        if($query === TRUE){
            echo 'Data Updated';
            return;
        }else{
            header("HTTP/1.0 500 Internal Server Error");
            echo $query;
            return;
        }
        header("HTTP/1.0 500 Internal Server Error");
        echo 'Data Not Updated';
        return;
    }

    public function disable_all()
    {
        if(!$this->table_normal OR !$this->enable_button_exist) {
            header("HTTP/1.0 500 Internal Server Error");
            echo "Data Not Updated.";
            return;
        }

        $query = $this->Crypto_model->disable_all();
        if($query === TRUE){
            echo 'Data Updated';
            return;
        }else{
            header("HTTP/1.0 500 Internal Server Error");
            echo $query;
            return;
        }
        header("HTTP/1.0 500 Internal Server Error");
        echo 'Data Not Updated';
        return;
    }

    public function update_start_stop()
    {
        if(!$this->table_normal OR !$this->enable_button_exist) {
            header("HTTP/1.0 500 Internal Server Error");
            echo "Data Not Updated.";
            return;
        }

        $params['id'] = $this->input->post('id');
        /*id (integer) check*/
        if($params['id'] == 'id'){
            if(!$this->is_number($this->input->post($params['id'])) OR $this->input->post($params['id']) < 0){
                header("HTTP/1.0 500 Internal Server Error");
                echo "Data Not Updated. ".$params['id']->name." invalid";
                return;
            }
        }

        $query = $this->Crypto_model->get_where($params);
        $params['enable'] = '';
        foreach($query->result() as $each){
            $enable = $each->enable;
            $params['enable'] = ($enable == '0')?1:0;
        }

        $query = $this->Crypto_model->update($params);
        if($query === TRUE){
            echo 'Data Updated';
        return;
        }else{
            header("HTTP/1.0 500 Internal Server Error");
            echo $query;
            return;
        }
    }


/*
    public function create()
    {
        $menu = $this->input->post('menu');
        $table = TABLE_PREFIX.$menu;
        $data['crypto_name'] = $this->uri->segment(2);
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 1,
                'auto_increment' => TRUE
            ),
            'symbol' => array(
                'type' => 'VARCHAR',
                'constraint' => '45',
                'unique' => TRUE,
            ),
            'base_c' => array(
                'type' =>'VARCHAR',
                'constraint' => '45',
            ),
            'enable' => array(
                'type' =>'TINYINT',
                'constraint' => '4',
                'null' => TRUE,
            ),
            'spread' => array(
                'type' =>'DECIMAL',
                'constraint' => '11,5',
                'default' => '0.00000',
            ),
            'max_qty_bid' => array(
                'type' =>'DECIMAL',
                'constraint' => '11,5',
                'default' => '0.00000',
            ),
            'max_qty_ask' => array(
                'type' =>'DECIMAL',
                'constraint' => '11,5',
                'default' => '0.00000',
            ),
            'account' => array(
                'type' =>'VARCHAR',
                'constraint' => '45',
                'null' => TRUE,
            ),
            'spread_hedge' => array(
                'type' =>'DECIMAL',
                'constraint' => '11,5',
                'default' => '0.20000',
            ),
            'counter_c' => array(
                'type' =>'VARCHAR',
                'constraint' => '45',
            ),
        );
        $this->dbforge->add_field($fields);

        $this->dbforge->add_key('id', TRUE);

        $attributes = array('ENGINE' => 'InnoDB', 'DEFAULT CHARSET' => 'latin1', 'COLLATE' => 'latin1_swedish_ci');

        if (!$this->dbforge->create_table($table, TRUE, $attributes))
        {
            echo "<script>alert('Can not insert table');</script>";
        }
        redirect(base_url().'crypto/'.$data['crypto_name']);

    }

    public function rename()
    {
        $old_menu = $this->input->post('old_menu');
        $new_menu = strtolower($this->input->post('new_menu'));
        $old_table = TABLE_PREFIX.$old_menu;
        $new_table = TABLE_PREFIX.$new_menu;
        $data['crypto_name'] = $this->uri->segment(2);

        $result = $this->dbforge->rename_table($old_table, $new_table);

        //success rename active menu
        if ($result && $old_menu == $data['crypto_name']){
            redirect(base_url().'crypto/'.$new_menu);
            return false;
        }

        //failed rename
        if(!$result){
            echo "<script>
            url = '".base_url()."crypto/".$data['crypto_name']."';
            alert('Can not rename table for ".$old_menu."');
            window.location.href = url;
            </script>";
            return false;
        }
        redirect(base_url().'crypto/'.$data['crypto_name']);
    }

    public function drop()
    {
        $menu = $this->input->post('menu');
        $table = TABLE_PREFIX.$menu;
        $data['all_table'] = $this->Crypto_model->get_all_table();
        $data['crypto_name'] = $this->uri->segment(2);

        //delete last table
        if(count($data['all_table']) == 1){
            echo "<script>
            url = '".base_url()."crypto/".$data['crypto_name']."';
            alert('Can not delete last table for ".$menu."');
            window.location.href = url;
            </script>";
            return false;
        }

        $result = $this->dbforge->drop_table($table);

        //success delete active menu
        if($result && $menu == $data['crypto_name']){
            $this->load->model('Crypto_model');
            $data['all_table'] = $this->Crypto_model->get_all_table();
            $crypto = substr(reset($data['all_table']),strlen(TABLE_PREFIX));
            redirect(base_url().'crypto/'.$crypto);
            return false;
        }

        //failed delete
        if(!$result){
            echo "<script>
            url = '".base_url()."crypto/".$data['crypto_name']."';
            alert('Can not delete menu for ".$menu."');
            window.location.href = url;
            </script>";
            return false;
        }

        redirect(base_url().'crypto/'.$data['crypto_name']);

    }
*/

}
