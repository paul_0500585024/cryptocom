<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'vendor/autoload.php');

class Balance extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		$this->load->library('cryptocom');
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$this->load->library('dataformat');
		$this->config->load('crypto_com', TRUE);
        $this->load->model('Starting_balance_model');		
        $this->initialize();
    }
	
	private function balance()
	{
		$query = $this->Starting_balance_model->get_balances();
		$balances = array();
		foreach($query->result() as $each){
			$balance_obj = new stdClass();
			foreach((array)$each as $key2=>$each2){
				if($key2 == 'id') continue;
				$balance_obj->$key2 = $each2;
			}
			$balances[] = $balance_obj;
		}
		return $balances;
	}

    private function initialize()
    {
//		var_dump($this->balance());exit();
//		$raw_data = $this->dataformat->display_balance_cryptocom_binance($this->balance());
//		$cryptocom_binance_data = $raw_data['data'];
//		var_dump($cryptocom_binance_data);exit();
		
		
		$gen_columns = array();

		$list_columns[] = 'coin';
		if(count($this->balance()) > 0){
			$list_columns[] = 'start_balance';
		}
		$list_columns[] = 'binance';
		$list_columns[] = 'cryptocom';
		$list_columns[] = 'current_balance';
		$list_columns[] = 'earned_balance';
		
		foreach($list_columns as $key=>$each){
			$column = new stdClass();
			$column->name = $each;
			$gen_columns[$key] = $column;
		}
		$this->columns = $gen_columns;
    }

	public function index()
	{
		$data['columns'] = $this->columns;
		$this->load->view("balance", $data);
	}
	
	public function fetch()
	{
        $params['draw'] = $this->input->post('draw');
        $params['columns'] = $this->input->post('columns');
        $params['start'] = $this->input->post('start');
        $params['length'] = $this->input->post('length');
        $params['search'] = $this->input->post('search');
        $params['order'] = $this->input->post('order');		

		$raw_data = $this->dataformat->display_balance_cryptocom_binance($this->balance());
		$cryptocom_binance_data = $raw_data['data'];

		$data = array();
		foreach ($cryptocom_binance_data as $key_binance=>$each) {
            $sub_array = array();
			foreach ($this->columns as $key_columns=>$each_col) {
				$col = $each_col->name;
				$sub_array[] = '<div class="update" data-column="' . $col . '">' . $each->$col . '</div>';				
			}
			$data[] = $sub_array;			
		}

		$record_total = count($cryptocom_binance_data);		

        $output = array(
            "draw"    => intval($_POST["draw"]),
            "recordsTotal"  =>  $record_total,
            "recordsFiltered" => $record_total,
            "data"    => $data
        );

        echo json_encode($output);
	}

	public function export_to_csv(){
		$raw_data = $this->dataformat->display_balance_cryptocom_binance($this->balance());
		$cryptocom_binance_data = $raw_data['data'];
		
		$encoded_new_coin_list_valued = json_encode($cryptocom_binance_data);
		jsonToCsv($encoded_new_coin_list_valued,false,true);		
	}
	
	public function export_to_excel(){
		$raw_data = $this->dataformat->display_balance_cryptocom_binance($this->balance());
		$cryptocom_binance_data = $raw_data['data'];
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("binance.com")
									->setDescription("balance binance.com");

		$objPHPExcel->setActiveSheetIndex(0);

		$row_number = '1';
		foreach($cryptocom_binance_data as $key_coin_list=>$each_new_coin_list){
			if($key_coin_list == 0){
				$counter = 0;
				$objPHPExcel->getActiveSheet()->getStyle("A$row_number:ZZ$row_number")->getFont()->setBold( true );
				foreach((array)$each_new_coin_list as $each_coin_list_key=>$each_coin_list_value){
					$objPHPExcel->getActiveSheet()->setCellValue((column_excel($counter).$row_number), $each_coin_list_key);
					$counter++;
				}				
			}
			
			$row_number++;
			$counter = 0;
			foreach((array)$each_new_coin_list as $each_coin_list_key=>$each_coin_list_value){
				$objPHPExcel->getActiveSheet()->setCellValue((column_excel($counter).$row_number), $each_coin_list_value);
				$counter++;
			}
		}
		
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="balancecom.xlsx"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');

		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
			
	}
		
}
