<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Balance_cryptocom extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		$this->load->library('cryptocom');
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');		
		$this->load->library('dataformat');
		$this->config->load('crypto_com', TRUE);
        $this->initialize();
    }

    private function initialize()
    {
		$gen_columns = array();
		
		$list_columns = array('coin','normal','locked','btcValuation');
		
		foreach($list_columns as $key=>$each){
			$column = new stdClass();
			$column->name = $each;
			$gen_columns[$key] = $column;
		}
		$this->columns = $gen_columns;
    }

	public function index()
	{
//		$crypto_com = $this->config->item('crypto_com');
//		$this->cryptocom->initialize($crypto_com);
//		$resp = $this->cryptocom->balance();
//		{"code":"100004","msg":"request parameter illegal","data":null}
//		{"code":"0","msg":"suc","data":{"total_asset":"4.25559761","coin_list":[{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"cro"},{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"mco"},{"normal":"16107.75","locked":"167.08","btcValuation":null,"coin":"usdt"},{"normal":"1.70334373","locked":"0.01017900","btcValuation":null,"coin":"btc"},{"normal":"3.81973660","locked":"0.00000000","btcValuation":null,"coin":"eth"},{"normal":"407.78000000","locked":"0.00000000","btcValuation":null,"coin":"xrp"},{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"ltc"},{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"eos"},{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"xlm"},{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"usdc"},{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"atom"},{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"link"},{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"xtz"}]}}
//		$resp = '{"code":"0","msg":"suc","data":{"total_asset":"4.25559761","coin_list":[{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"cro"},{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"mco"},{"normal":"16107.75","locked":"167.08","btcValuation":null,"coin":"usdt"},{"normal":"1.70334373","locked":"0.01017900","btcValuation":null,"coin":"btc"},{"normal":"3.81973660","locked":"0.00000000","btcValuation":null,"coin":"eth"},{"normal":"407.78000000","locked":"0.00000000","btcValuation":null,"coin":"xrp"},{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"ltc"},{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"eos"},{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"xlm"},{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"usdc"},{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"atom"},{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"link"},{"normal":"0.00000000","locked":"0.00000000","btcValuation":null,"coin":"xtz"}]}}';
//		$resp_obj = json_decode($resp);

//		if($resp_obj->code == "0"){
//		}else{
//			echo "Error = ".$resp_obj->code;
//			echo "<br>";
//			echo "Message = ".$resp_obj->msg;
//		}
		$data['columns'] = $this->columns;
		$this->load->view("balance_cryptocom", $data);
	}
	
	public function fetch()
	{
        $params['draw'] = $this->input->post('draw');
        $params['columns'] = $this->input->post('columns');
        $params['start'] = $this->input->post('start');
        $params['length'] = $this->input->post('length');
        $params['search'] = $this->input->post('search');
        $params['order'] = $this->input->post('order');

		$raw_data = $this->dataformat->cryptocom();
		$cryptocom_data = $raw_data['data'];
		$cryptocom_total_asset = isset($raw_data['total_asset'])?$raw_data['total_asset']:'';
		
		$data = array();
		foreach ($cryptocom_data as $key_cryptocom=>$each) {
			$sub_array = array();
			foreach ($this->columns as $key_columns=>$each_col) {
				$col = $each_col->name;
				$sub_array[] = '<div class="update" data-column="' . $col . '">' . $each->$col . '</div>';				
			}
			$data[] = $sub_array;			
		}

		$record_total = count($cryptocom_data);

        $output = array(
            "draw"    => intval($_POST["draw"]),
            "recordsTotal"  =>  $record_total,
            "recordsFiltered" => $record_total,
            "data"    => $data,
			"total_asset" => $cryptocom_total_asset,
        );

        echo json_encode($output);
	}
	
	public function export_to_csv(){
		$raw_data = $this->dataformat->cryptocom();
		$cryptocom_data = $raw_data['data'];
		
		$encoded_new_coin_list_valued = json_encode($cryptocom_data);
		jsonToCsv($encoded_new_coin_list_valued,false,true);		
	}
	
	public function export_to_excel(){
		$raw_data = $this->dataformat->cryptocom();
		$cryptocom_data = $raw_data['data'];
		$cryptocom_total_asset = isset($raw_data['total_asset'])?$raw_data['total_asset']:'';
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("crypto.com")
									->setDescription("balance cyrpto.com");

		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle("C1")->getFont()->setBold( true );

		$objPHPExcel->setActiveSheetIndex()->setCellValue('A1', "Total Asset");
		$objPHPExcel->setActiveSheetIndex()->setCellValue('C1', $cryptocom_total_asset);											
		
		$row_number = '3';
				
		foreach($cryptocom_data as $key_coin_list=>$each_new_coin_list){
			if($key_coin_list == 0){
				$counter = 0;
				$objPHPExcel->getActiveSheet()->getStyle("A$row_number:ZZ$row_number")->getFont()->setBold( true );
				foreach((array)$each_new_coin_list as $each_coin_list_key=>$each_coin_list_value){
					$objPHPExcel->getActiveSheet()->setCellValue((column_excel($counter).$row_number), $each_coin_list_key);
					$counter++;
				}				
			}

			$row_number++;
			$counter = 0;
			foreach((array)$each_new_coin_list as $each_coin_list_key=>$each_coin_list_value){
				$objPHPExcel->getActiveSheet()->setCellValue((column_excel($counter).$row_number), $each_coin_list_value);
				$counter++;
			}
		}					

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="cryptocom.xlsx"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');

		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}

}
