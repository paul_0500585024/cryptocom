<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li <?php echo ($this->uri->segment(1) == 'crypto')?'class="active"':''?>><a href="<?php echo base_url().'crypto/symbol'?>">DB Crypto <span class="sr-only">(current)</span></a></li>
        <li <?php echo ($this->uri->segment(1) == 'balance_cryptocom')?'class="active"':''?>><a href="<?php echo base_url().'balance_cryptocom'?>">Balance Crypto.com</a></li>
        <li <?php echo ($this->uri->segment(1) == 'balance_binance')?'class="active"':''?>><a href="<?php echo base_url().'balance_binance'?>">Balance Binance</a></li>
        <li <?php echo ($this->uri->segment(1) == 'balance')?'class="active"':''?>><a href="<?php echo base_url().'balance'?>">Balance</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
