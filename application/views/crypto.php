<!DOCTYPE html>
<html>
<head>
    <title><?php echo strtoupper($crypto_name)?></title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <style>
        body
        {
            margin:0;
            padding:0;
            background-color:#f1f1f1;
        }
        .box
        {
            background-color:#fff;
            border:1px solid #ccc;
            border-radius:5px;
            margin:25px;
            box-sizing:border-box;
        }
        .dataTables_filter {
            text-align: right !important;
        }
		.alert {
			padding: 5px;
			margin-bottom: 15px;
			border: 1px solid transparent;
			border-radius: 4px;
		}
		th:not(:last-child),td:not(:last-child){
			width:<?php echo ((100/(count($columns)+1))-0.5)?>%;
		}
		th:not(:last-child),td:not(:last-child){
			word-break:break-all;
		}
    </style>
</head>
<body>
<?php require_once('nav.php');?>
<div class="container-fluid box">
    <!-- Modal Add menu -->
    <div id="addMenu" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <form id="frm_add_menu" action="<?php echo $crypto_name?>/create" method="POST">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Menu</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="menuName">Menu</label>
                            <input type="text" class="form-control" id="menu" placeholder="Enter Menu" aria-describedby="menuHelp" name="menu" />
                            <small id="menuHelp" class="form-text text-muted">This will create a new table at Moscow Database</small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info">Create Menu</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <!-- Modal Rename menu -->
    <div id="RenameMenu" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <form id="frm_rename_menu" action="<?php echo $crypto_name?>/rename" method="POST">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Rename Menu</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="menuName">Menu</label>
                            <?php if(count($all_table) > 0):?>
                                <select class="form-control old_menu" name="old_menu">
                                    <?php foreach($all_table as $each):?>
                                        <?php $crypto = substr($each,strlen(TABLE_PREFIX));?>
                                        <option value="<?php echo $crypto?>" <?php echo ($crypto == $this->uri->segment(2) ? 'selected' : '' )?>><?php echo strtoupper($crypto);?></option>
                                    <?php endforeach;?>
                                </select>
                            <?php endif;?>
                        </div>
                        <div class="form-group">
                            <label for="newMenuName">New Menu For <span class="old_menu_name"></span></label>
                            <input type="text" class="form-control" id="new_menu" placeholder="Enter new menu" aria-describedby="menuHelp" name="new_menu" />
                            <small id="newmenuHelp" class="form-text text-muted">This will rename a new table at moscow2 Database</small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-warning">Rename Menu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Delete menu -->
    <div id="DropMenu" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <form id="frm_delete_menu" action="<?php echo $crypto_name?>/drop" method="POST">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Menu</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="menuName">Menu</label>
                            <?php if(count($all_table) > 0):?>
                                <select class="form-control" name="menu">
                                    <?php foreach($all_table as $each):?>
                                        <?php $crypto = substr($each,strlen(TABLE_PREFIX));?>
                                        <option value="<?php echo $crypto?>" <?php echo ($crypto == $this->uri->segment(2) ? 'selected' : '' )?>><?php echo strtoupper($crypto);?></option>
                                    <?php endforeach;?>
                                </select>
                            <?php endif;?>
                            <small id="menuHelp" class="form-text text-muted">This will drop table at Moscow 2 UI Symbol Database</small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Delete Menu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

	<div class="row">
		<div class="col-md-12">
			<center><h1><?php echo strtoupper($crypto_name);?></h1></center>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<?php if($table_normal):?>
				<div class="btn-group" style="margin-right:10px" role="group" aria-label="Add Data">
					<button type="button" name="add" id="add" class="btn btn-info">Add Data <i class="fa fa-plus" aria-hidden="true"></i></button>
				</div>
			<?php endif;?>
			<?php if($table_normal && $enable_button_exist):?>
				<div class="btn-group" style="margin-right:10px" role="group" aria-label="Disable All">
					<button type="button" name="disable_all" id="disable_all" class="btn btn-danger">Disable All</button>
				</div>
			<?php endif;?>
			<?php if($table_normal && $enable_button_exist):?>
				<div class="btn-group" style="margin-right:10px" role="group" aria-label="Enable All">
					<button type="button" name="enable_all" id="enable_all" class="btn btn-warning">Enable All</button>
				</div>
			<?php endif;?>
            <div class="btn-group pull-right" role="group" style="margin-right:20px" aria-label="Enable Data">
                <div class="form-group">
                    <?php if(count($all_table) > 0):?>
                        <select class="form-control" id="menu" onchange="handleRedirect(this.value);">
                            <?php foreach($all_table as $each):?>
                                <?php $crypto = substr($each,strlen(TABLE_PREFIX));?>
                                <option value="<?php echo $crypto?>" <?php echo ($crypto == $this->uri->segment(2) ? 'selected' : '' )?>><?php echo strtoupper($crypto);?></option>
                            <?php endforeach;?>
                        </select>
                    <?php endif;?>
                </div>
            </div>
		</div>
		<div class="col-md-6">
<?php /*                <div class="btn-group pull-left" style="margin-right:10px" role="group" aria-label="Add Menu">
				<button type="button" name="add_menu" id="add_menu" class="btn btn-info" data-toggle="modal" data-target="#addMenu">Add Menu <i class="fa fa-plus" aria-hidden="true"></i></button>
			</div>
			<div class="btn-group" style="margin-right:10px" role="group" aria-label="Rename Menu">
				<button type="button" name="rename_menu" id="rename_menu" class="btn btn-warning" data-toggle="modal" data-target="#RenameMenu">Rename Menu</button>
			</div>
			<div class="btn-group" style="margin-right:10px" role="group" aria-label="Drop Menu">
				<button type="button" name="drop_menu" id="drop_menu" class="btn btn-danger" data-toggle="modal" data-target="#DropMenu">Delete Menu</button>
			</div> */?>
			<div class="btn-group pull-right">
				<button type="button" name="logout" id="logout" class="btn btn-info" onclick="window.location.href='<?php echo base_url()?>main/logout';">Logout</button>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div id="alert_message">
				<div class="alert">&nbsp;</div>
			</div>
		</div>
	</div>	

	<div class="row">
		<div class="col-lg-12">
			<?php if(count($columns)>0):?>
			<table id="user_data" class="table table-bordered table-striped" style="width:100%">
				<thead>
				<tr>
					<?php foreach($columns as $each_col): ?>
						<th><?php echo $each_col->name ?></th>
					<?php endforeach;?>
					<?php if($table_normal):?>
					<th></th>
					<?php endif;?>
				</tr>
				</thead>
			</table>
			<?php endif;?>
		</div>
	</div>	
</div>

<script type="text/javascript" language="javascript" >
    var url = '<?php echo base_url()?>';
    var uri = '<?php echo $crypto_name?>' + '/';
    var main_controller = 'crypto/';
    function handleRedirect(val){
        location = val;
    }

    function is_decimal(val, precision ,scale){
        var retval = false;
        var regex = "^[0-9]{1,"+(precision-scale)+"}(\.[0-9]{1,"+scale+"})?$";
        var re = new RegExp(regex,"g");
        if(val.match(re) !== null){
            retval = true;
        }
        return retval;
    }

    $(document).ready(function(){

        fetch_data();

        function fetch_data()
        {
            var dataTable = $('#user_data').DataTable({
                "processing" : true,
                "serverSide" : true,
                <?php if(count($columns)>0):?>
                    "columnDefs" : [
                        <?php foreach($columns as $inc=>$each_col): ?>
                            { "targets": [<?php echo $inc?>] },
                        <?php endforeach;?>
                        <?php if($table_normal):?>
                            <?php $inc++;?>
                                { "targets": [<?php echo $inc?>], "orderable": false },
                        <?php endif;?>
                    ],
                <?php endif;?>
                "order" : [],
                "ajax" : {
                    url: uri + "fetch",
                    type:"POST"
                },
				"scrollX": true
            });
        }

    <?php if($table_normal):?>
        function update_data(id, column_name, value)
        {
            $.ajax({
                url: uri + "update",
                method:"POST",
                data:{id:id, column_name:column_name, value:value},
                success:function(data)
                {
                    $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
                    $('#user_data').DataTable().destroy();
                    fetch_data();
                },
                error:function(err)
                {
                    alert(err.responseText);
                }
            });
            setInterval(function(){
                $('#alert_message').html('<div class="alert">&nbsp;</div>');
            }, 5000);
        }
    <?php endif;?>

        /*        $(document).on('blur', '.update', function(){
                    var id = $(this).data("id");
                    var column_name = $(this).data("column");
                    var value = $(this).text();
                    update_data(id, column_name, value);
                });*/

    <?php if($table_normal):?>
        $('#add').click(function(){
            var html = '';
            <?php if(count($columns)>0):?>
                html += '<tr>';
                <?php foreach($columns as $inc=>$each_col): ?>
                    <?php $col = $each_col->name;?>
                    <?php
                    switch($col) {
                        case 'enable':
                            echo "html += '<td><input type=\"checkbox\" id=\"data".$inc."\" /></td>';";
                            break;
                        default:
                            echo "html += '<td contenteditable id=\"data".$inc."\"></td>';";
                    }?>
                <?php endforeach;?>
                <?php $inc++;?>
                html += '<td><button type="button" name="insert" id="insert" class="btn btn-success btn-xs">Insert</button></td>';
                html += '</tr>';
            <?php endif;?>
            $('#user_data tbody').prepend(html);
        });
    <?php endif;?>

    <?php if($table_normal && $enable_button_exist):?>
        $('#disable_all').click(function(){
            var q = confirm("Are you sure want to disable all fields?");
            if(q == true) {
                $.ajax({
                    url: uri + "disable_all",
                    method: "GET",
                    success: function (data) {
                        $('#alert_message').html('<div class="alert alert-success">' + data + '</div>');
                        $('#user_data').DataTable().destroy();
                        fetch_data();
                    },
                    error: function (err) {
                        alert(err.responseText);
                    }
                });
            }
        });
    <?php endif;?>

        $('#frm_rename_menu').submit(function(){
            var menu_selected = $('#RenameMenu select option:selected').text();
            var q = confirm("Are you sure want to rename menu "+menu_selected+"?");
            if(q == true){
                return true;
            }
            return false;
        });

    <?php if($table_normal && $enable_button_exist):?>
        $('#enable_all').click(function(){
            var q = confirm("Are you sure want to enable all fields?");
            if(q == true){
                $.ajax({
                    url: uri + "enable_all",
                    method:"GET",
                    success:function(data)
                    {
                        $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
                        $('#user_data').DataTable().destroy();
                        fetch_data();
                    },
                    error:function(err)
                    {
                        alert(err.responseText);
                    }
                });
            }
        });
    <?php endif;?>

        $('#frm_delete_menu').submit(function(){
            var menu_selected = $('#DropMenu select option:selected').text();
            var q = confirm("Are you sure want to delete menu "+menu_selected+"?");
            if(q == true){
                return true;
            }
            return false;
        });

    <?php if($table_normal):?>
        $(document).on('click', '#insert', function(){
            <?php /*initialize variable*/?>
            <?php foreach($columns as $inc=>$each_col): ?>
            <?php $col = $each_col->name?>
            <?php
            switch($col) {
                case 'enable':
                    echo "var ".$col." = +$('#data".$inc."').is(':checked');";
                    echo "enable = enable.toString();";
                    break;
                default:
                    echo "var ".$col." = $('#data".$inc."').text();";
            }?>
            <?php endforeach;?>

            <?php /*checking variable*/?>
            <?php foreach($columns as $inc=>$each_col): ?>
                <?php
				echo "if(!(".$each_col->nullable." == 1 && ".$each_col->name." == '')){";
					/*required field to be filled*/
					echo "if(".$each_col->name." == ''){alert('".$each_col->name." are required'); return false;}";

					/*boolean check*/
					if($each_col->name == 'enable'){
						echo "if(jQuery.inArray(".$each_col->name.", ['0','1'] ) < 0){alert('".$each_col->name." should 0 or 1');return false;};";
					}
					/*var_char or char length checked*/
					elseif($each_col->type == 'varchar' OR $each_col->type == 'char'){
						echo "if(".$each_col->max_length. " < " . $each_col->name.".length ){alert('".$each_col->name." not more than ".$each_col->max_length."'); return false;}";
					}
					/*decimal check*/
					elseif($each_col->type == 'decimal'){
						echo "if(is_decimal(".$each_col->name.",".$each_col->max_length.",".$each_col->numeric_scale.") === false){alert('".$each_col->name." should decimal with length ".($each_col->max_length - $each_col->numeric_scale)." digit And comma length ".$each_col->numeric_scale." digit');return false;}";
					}
				echo "}";				
                ?>
            <?php endforeach;?>

            <?php $data = '';?>
            <?php $end_column = count($columns) - 1 ;?>
            <?php foreach($columns as $inc=>$each_col): ?>
                <?php $data .= $each_col->name.':'.$each_col->name;?>
                <?php if($inc != $end_column) {$data .= ',';}?>
            <?php endforeach;?>
            $.ajax({
                url: uri + "insert",
                method:"POST",
                data:{<?php echo $data?>},
                success:function(data)
                {
                    $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
                    $('#user_data').DataTable().destroy();
                    fetch_data();
                },
                error:function(err)
                {
                    alert(err.responseText);
                }
            });
            setInterval(function(){
                $('#alert_message').html('<div class="alert">&nbsp;</div>');
            }, 5000);
        });
    <?php endif;?>

    <?php if($table_normal):?>
        $(document).on('click', '.delete', function(){
            var id = $(this).attr("id");
            if(confirm("Are you sure you want to remove this?"))
            {
                $.ajax({
                    url: uri + "delete",
                    method:"POST",
                    data:{id:id},
                    success:function(data){
                        $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
                        $('#user_data').DataTable().destroy();
                        fetch_data();
                    },
                    error:function(err)
                    {
                        alert(err.responseText);
                    }
                });
                setInterval(function(){
                    $('#alert_message').html('<div class="alert">&nbsp;</div>');
                }, 5000);
            }
        });
    <?php endif;?>

    <?php if($table_normal):?>
        $(document).on('click', '.edit', function(){
            var id = $(this).attr("id");
			
            /*other edit action element*/
            $("button.canceledit[id!="+id+"]").text('Edit').removeClass("btn-danger").removeClass("canceledit").addClass("btn-info").addClass("edit");
            $("button.save[id!="+id+"]").prop('disabled', true);
            $("div[data-id!="+id+"][contenteditable=true]").removeAttr("style").attr('contenteditable','false');
            $("input:checkbox[data-id!="+id+"]").prop('disabled', true);

            /*edit action element*/
            $(this).text('Cancel Edit').removeClass('btn-info').removeClass('edit').addClass('btn-danger').addClass('canceledit');
            $("button.save[id="+id+"]").prop('disabled', false);
            $("div[data-id="+id+"]").attr('contenteditable','true').css({"border-color": "#C1E0FF",
                "border-width":"1px",
                "border-style":"solid", 
				"width":$("div[data-id="+id+"]").width()-2,
				"max-width":$("div[data-id="+id+"]").width()-2, 
				"outline":"none",
				"white-space":"pre-wrap",
				"white-space":"-moz-pre-wrap",
				"white-space":"-pre-wrap",
				"white-space":"-o-pre-wrap",
				"word-wrap":"break-word",
			});
            $("input:checkbox[data-id="+id+"]").removeAttr('disabled');
        });
    <?php endif;?>

    <?php if($table_normal):?>
        $(document).on('click', '.edit_form', function(){
            var id = $(this).attr("id");
            window.location.href = url+main_controller+uri+'edit_form/'+id;
        });
    <?php endif;?>


    <?php if($table_normal):?>
        $(document).on('click', '.canceledit', function() {
            $('#user_data').DataTable().destroy();
            fetch_data();
            var id = $(this).attr("id");

            $(this).text('Edit').removeClass('btn-danger').removeClass('canceledit').addClass('btn-info').addClass('edit');
            $("button.save[id="+id+"]").prop('disabled', true);
            $("div[data-id="+id+"]").removeAttr("style").attr('contenteditable','false');
            $("input:checkbox[data-id="+id+"]").prop('disabled', true);
//            $("button.save[id="+id+"]").prop('disabled', false);

        });
    <?php endif;?>

    <?php if($table_normal):?>
        $(document).on('click', '.start_stop', function(){
            var id = $(this).attr("id");
            <?php $data = 'id:id,';?>
            $.ajax({
                url: uri + "update_start_stop",
                method:"POST",
                data:{<?php echo $data?>},
                success:function(data)
                {
                    $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
                    $('#user_data').DataTable().destroy();
                    fetch_data();
                },
                error:function(err)
                {
                    alert(err.responseText);
                }
            });
            setInterval(function(){
                $('#alert_message').html('<div class="alert">&nbsp;</div>');
            }, 5000);
        });
    <?php endif;?>

    <?php if($table_normal):?>
        $(document).on('click', '.save', function(){
            var id = $(this).attr("id");
            <?php /*initialize variable*/?>
            <?php foreach($columns as $inc=>$each_col): ?>
            <?php $col = $each_col->name?>
            <?php
            switch($col) {
                case 'enable':
                    echo "var ".$col." = +$('input[data-id='+id+']').is(':checked');";
                    echo "enable = enable.toString();";
                    break;
                default:
                    echo "var ".$col." = $('div[data-id='+id+'][data-column=".$col."]').text();";
            }
			?>
            <?php endforeach;?>

            <?php /*checking variable*/?>
            <?php foreach($columns as $inc=>$each_col): ?>
            <?php
			echo "if(!(".$each_col->nullable." == 1 && ".$each_col->name." == '')){";
				/*required field to be filled*/
				echo "if(".$each_col->name." == ''){alert('".$each_col->name." are required'); return false;}";

				/*boolean check*/
				if($each_col->name == 'enable'){
					echo "if(jQuery.inArray(".$each_col->name.", ['0','1'] ) < 0){alert('".$each_col->name." should 0 or 1');return false;};";
				}
				/*var_char or char length checked*/
				elseif($each_col->type == 'varchar' OR $each_col->type == 'char'){
					echo "if(".$each_col->max_length. " < " . $each_col->name.".length ){alert('".$each_col->name." not more than ".$each_col->max_length."'); return false;}";
				}
				/*decimal check*/
				elseif($each_col->type == 'decimal'){
					echo "if(is_decimal(".$each_col->name.",".$each_col->max_length.",".$each_col->numeric_scale.") === false){alert('".$each_col->name." should decimal with length ".($each_col->max_length - $each_col->numeric_scale)." digit And comma length ".$each_col->numeric_scale." digit');return false;}";
				}
			echo "}";
            ?>
            <?php endforeach;?>

            <?php $data = 'id:id,';?>
            <?php $end_column = count($columns) - 1 ;?>
            <?php foreach($columns as $inc=>$each_col): ?>
            <?php $data .= $each_col->name.':'.$each_col->name;?>
            <?php if($inc != $end_column) {$data .= ',';}?>
            <?php endforeach;?>

            $.ajax({
                url: uri + "update",
                method:"POST",
                data:{<?php echo $data?>},
                success:function(data)
                {
                    $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
                    $('#user_data').DataTable().destroy();
                    fetch_data();
                },
                error:function(err)
                {
                    alert(err.responseText);
                }
            });
        });
    <?php endif;?>

        $('div#RenameMenu .old_menu').change(function(){
            $('#RenameMenu .old_menu_name').html($(this).find('option:selected').text());
        });

        $("#RenameMenu").on("shown.bs.modal", function(e) {
            $('div#RenameMenu .old_menu_name').html($('#RenameMenu .old_menu option:selected').text());
        })
    });
</script>
</body>
</html>