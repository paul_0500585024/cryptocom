<!DOCTYPE html>
<html>
<head>
    <title>Balance Binance</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <style>
        body
        {
            margin:0;
            padding:0;
            background-color:#f1f1f1;
        }
        .box
        {
            background-color:#fff;
            border:1px solid #ccc;
            border-radius:5px;
            margin:25px;
            box-sizing:border-box;
        }
        .dataTables_filter {
            text-align: right !important;
        }
		.alert {
			padding: 5px;
			margin-bottom: 15px;
			border: 1px solid transparent;
			border-radius: 4px;
		}
		th:not(:last-child),td:not(:last-child){
			width:<?php echo ((100/(count($columns)+1))-0.5)?>%;
		}
		th:not(:last-child),td:not(:last-child){
			word-break:break-all;
		}
    </style>
</head>
<body>
<?php require_once('nav.php');?>
<div class="container-fluid box">
	<div class="row">
		<span class="pull-right" style="margin-top:10px;margin-right:10px">
			<button type="button" class="btn btn-info" onclick="export_to_csv()">Export To CSV</button>
			<button type="button" class="btn btn-danger" onclick="export_to_excel()">Export To Excel</button>
		</span>
	</div>
	<div class="row">
		<div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 col-xs-12"><h3>Balance Binance</h3></div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<?php if(count($columns)>0):?>
			<table id="user_data" class="table table-bordered table-striped" style="width:100%">
				<thead>
				<tr>
					<?php foreach($columns as $each_col): ?>
						<th><?php echo $each_col->name ?></th>
					<?php endforeach;?>
				</tr>
				</thead>
			</table>
			<?php endif;?>
		</div>
	</div>
</div>
<script type="text/javascript" language="javascript" >
    var uri = '<?php echo "balance_binance"?>' + '/';
	$(document).ready(function(){

        fetch_data();

        function fetch_data()
        {
            var dataTable = $('#user_data').DataTable({
				"paging": false,
				"searching": false,
                "processing" : true,
                "serverSide" : true,
				"ordering": false,
                <?php if(count($columns)>0):?>
                    "columnDefs" : [
                        <?php foreach($columns as $inc=>$each_col): ?>
                            { "targets": [<?php echo $inc?>] },
                        <?php endforeach;?>
                    ],
                <?php endif;?>
                "order" : [],
                "ajax" : {
                    url: uri + "fetch",
                    type:"POST",
                },
				"initComplete":function( settings, json){					
					$('#total_asset').html(json.total_asset);
					// call your function here
				},
				"scrollX": true
            });
			setInterval( function () {
				dataTable.ajax.reload();
			}, 30000 );		
        }
	});
	function export_to_csv(){
		location.href = uri + "export_to_csv";
	}
	function export_to_excel(){
		location.href = uri + "export_to_excel";
	}

</script>
</body>
</html>