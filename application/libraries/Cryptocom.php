<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cryptocom {
	
	public $timeout = 1000;
	
	public $apiurl = "https://api.crypto.com";
	
	public $apikey = "";
	
	public $apisec = "";

	public function __construct(array $config = array())
	{	
		$CI = & get_instance();
		$CI->load->helper('moscow_ui2_symbol_helper');	
		$this->initialize($config);
		
	}

	public function initialize(array $config = array())
	{
		foreach ($config as $key => $val)
		{
			if (isset($this->$key))
			{
				$method = 'set_'.$key;
				

				if (method_exists($this, $method))
				{
					$this->$method($val);
				}
				else
				{
					$this->$key = $val;
				}
			}
		}

		return $this;
	}
		
	public function set_timeout($val)
	{
		$this->timeout = $val;
	}
	
	public function set_apiurl($val)
	{
		$this->apiurl = $val;		
	}

	public function set_apikey($val)
	{
		$this->apikey = $val;		
	}

	public function set_apisec($val)
	{
		$this->apisec = $val;		
	}


	public function get_timeout()
	{
		return $this->timeout;
	}
	
	public function get_apiurl()
	{
		return $this->apiurl;		
	}

	public function get_apikey()
	{
		return $this->apikey;		
	}

	public function get_apisec()
	{
		return $this->apisec;		
	}
	
/*	public function api_key_get()
	{
		$ch = curl_init(); 

		curl_setopt($ch, CURLOPT_URL, "https://www.google.com/");

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                            'Content-Type: application/x-www-form-urlencoded'
                                            ));
		$output = curl_exec($ch); 

		curl_close($ch);      

		echo $output;		
	}
	*/


	public function balance()
	{
		$url = $this->apiurl . "/v1/account";
		return $this->api_key_post($url);
	}
	
	private function api_key_post($url)
	{
		$params = array();
        $params["api_key"] = $this->apikey;
        $params["time"] = get_timestamp();
        $params["sign"] = $this->create_sign($params);
        return $this->http_post($url, $params);		
	}
	
	private function create_sign($params)
	{
		$s = implode('',array_map('join_into_text',array_keys($params),array_values($params))).$this->apisec;
		$s = utf8_encode($s);
		$h = hash('sha256',$s);
		return $h;
	}
	
	private function http_post($url, $params){
		$fields_string = '';
		foreach($params as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		$fields_string = rtrim($fields_string, '&');
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($params));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                            'Content-Type: application/x-www-form-urlencoded'
                                            ));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		//execute post
		$result = curl_exec($ch);

		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		//close connection
		curl_close($ch);
		
		if($httpcode == 200){
			return $result; 
		}else{
			return '{"code": -1, "msg": "response status:'.$httpcode.'"}';
		}		
	}
	
/*	    def http_post(self, url, params):
        headers = {
            "Content-type": "application/x-www-form-urlencoded",
        }
        data = urllib.parse.urlencode(params or {})
        try:
            response = requests.post(url, data, headers=headers, timeout=self.timeout)
            if response.status_code == 200:
                return response.json()
            else:
                return {"code": -1, "msg": "response status:%s" % response.status_code}
        except Exception as e:
            print("httpPost failed, detail is:%s" % e)
            return {"code": -1, "msg": e}*/

		
/*    def create_sign(self, params):
        sorted_params = sorted(params.items(), key=lambda d: d[0], reverse=False)
        s = "".join(map(lambda x: str(x[0]) + str(x[1] or ""), sorted_params)) + self.apisec
        s = s.encode('utf-8')
        h = hashlib.sha256(s)
        return h.hexdigest()*/
	


}