<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Starting_balance_model extends CI_Model
{
    public function __construct()
    {
    }
	
	public function get_balances()
	{
		$this->db->select('LOWER(currency) AS coin, balance AS start_balance',FALSE);
		$this->db->from('starting_balance');
		$query = $this->db->get();
		return $query;
	}

}