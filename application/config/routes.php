<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main';
$route['crypto/(:any)'] = 'crypto';
$route['crypto/(:any)/fetch'] = 'crypto/fetch';
$route['crypto/(:any)/insert'] = 'crypto/insert';
$route['crypto/(:any)/update'] = 'crypto/update';
$route['crypto/(:any)/delete'] = 'crypto/delete';
$route['crypto/(:any)/enable_all'] = 'crypto/enable_all';
$route['crypto/(:any)/disable_all'] = 'crypto/disable_all';
$route['crypto/(:any)/create'] = 'crypto/create';
$route['crypto/(:any)/rename'] = 'crypto/rename';
$route['crypto/(:any)/drop'] = 'crypto/drop';
$route['crypto/(:any)/update_start_stop'] = 'crypto/update_start_stop';
$route['crypto/(:any)/edit_form'] = 'crypto/edit_form';
$route['crypto/(:any)/edit_form/(:any)'] = 'crypto/edit_form';

$route['balance_cryptocom'] = 'balance_cryptocom';
$route['balance_cryptocom/fetch'] = 'balance_cryptocom/fetch';
$route['balance_cryptocom/export_to_csv'] = 'balance_cryptocom/export_to_csv';
$route['balance_cryptocom/export_to_excel'] = 'balance_cryptocom/export_to_excel';

$route['balance_binance'] = 'balance_binance';
$route['balance_binance/fetch'] = 'balance_binance/fetch';
$route['balance_binance/export_to_csv'] = 'balance_binance/export_to_csv';
$route['balance_binance/export_to_excel'] = 'balance_binance/export_to_excel';

$route['balance'] = 'balance';
$route['balance/fetch'] = 'balance/fetch';
$route['balance/export_to_csv'] = 'balance/export_to_csv';
$route['balance/export_to_excel'] = 'balance/export_to_excel';


//$route['404_override'] = 'main/logout';
$route['translate_uri_dashes'] = FALSE;
